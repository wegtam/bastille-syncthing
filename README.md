# Syncthing template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[Syncthing](https://syncthing.net/) service inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of Syncthing (see
[Bastillefile](Bastillefile)) will be installed and the service will be
configured to allow network access.

The external TCP port for Syncthing can be configured via the
`PORT` argument and it defaults to `22000`.

An external directory which is defined via `SYNC_DIR` is expected to exist
and will be mounted into the jail to `/srv/syncthing` which is also the
default path for the host directory. The directory will be mounted via
nullfs with write permissions.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

So far bastille only supports downloading from GitHub or GitLab, so you have
to fetch the template manually:

```
# mkdir <your-bastille-template-dir>/wegtam
# git -C <your-bastille-template-dir>/wegtam clone https://codeberg.org/wegtam/bastille-syncthing.git
```

## Usage

Please note that you will have to make some adjustments to the configuration
after you have started the service!

### 1. Install with default settings

```
# bastille template TARGET wegtam/bastille-syncthing
```

### 2. Install with custom settings

```
# bastille template syncthing wegtam/bastille-syncthing --arg PORT=22001 --arg SYNC_DIR=/home/shared
```
